North Robots Example
====================

**WARNING: This is a semi-realistic example that will not work on a real robot without more work**

This project is an example of how you can structure Python applications for automated chemistry work.

The architecture is broken down into the following layers:

Devices
-------

The `devices` module contains classes that control various devices that are custom to the current experiment setup.
Device classes should be self-contained so they can be used without any of the other parts of the application. The
`north_devices` package contains a number of pre-written device classes that can be used in your application.

Deck
----

The `Deck` (`config/deck.py`) is a class that sets up the C9, N9 and any other devices needed for the experiment. An
instance of this `Deck` class needs to be "injected" (passed into) the station classes so they can access the robot and
other devices.

Sample
------

The `Sample` (`data/sample.py`) class is a container for data that changes for each iteration of the experiment. In most
cases a list of `Sample`s will be created for the main script to use. The main script will loop through the samples and
process them using `Station`s. Once all samples have been processed, the experiment is finished.

Stations
--------

The `stations` module contains classes that can control the robot to perform different actions needed to process the
current sample. Each station class should have a `Deck` instance injected into the constructor that can be used to
control the robot or other devices.

main.py
-------

This is the main entry point of the application. It creates the `Deck` and station instances, along with a list of
`Sample` instances that need to be processed for the experiment. It also contains the main loop for the program, which
loops through the samples and performs different actions using the various stations.
