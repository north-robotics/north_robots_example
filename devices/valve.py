from ftdi_serial import Serial


class Valve:
    """
    This is an example of how to use a custom component that works with a serial port. The ``ftdi_serial`` library
    is a PySerial-compatible library for FTDI devices that allows you to connect with a serial number instead of a
    COM port for better reliability (serial numbers don't change if you plug the device into a different port).
    """

    def __init__(self, device_serial=None):
        self.serial = Serial(device_serial=device_serial, baudrate=9600)

    def switch_a(self):
        self.serial.write(b'GOA')

    def switch_b(self):
        self.serial.write(b'GOB')
