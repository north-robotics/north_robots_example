class Sample:
    """
    The Sample class is a simple container for data needed for an iteration of the experiment
    """

    def __init__(self, sample_index, stock_solution_index, stock_volume_ml, weight=0.0):
        self.sample_index = sample_index
        self.stock_solution_index = stock_solution_index
        self.stock_volume_ml = stock_volume_ml
        self.weight = weight
