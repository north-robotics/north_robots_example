from north_c9.controller import C9Controller
from north_robots.n9 import N9Robot
from north_devices.pumps.tecan_cavro import TecanCavro
from devices.valve import Valve


class Deck:
    """
    The Deck class sets up the robot and other devices needed for the experiment
    """

    def __init__(self, c9_serial=None):
        self.c9 = C9Controller(device_serial=c9_serial)
        self.n9 = N9Robot(self.c9)
        self.pump = TecanCavro(self.c9.com(0, baudrate=9600), address=0)
        self.valve = Valve()
