from config.deck import Deck
from data.sample import Sample
from stations.weigh import WeighStation
from stations.needle import NeedleStation
from stations.sample import SampleStation
from stations.stock_solution import StockSolutionStation


# create an instance of the deck, which sets the robot and other needed devices
deck = Deck()

# setup stations
weigh_station = WeighStation(deck)
needle_station = NeedleStation(deck)
# these stations work with the needle station, so we need to "inject" them into the constructor
sample_station = SampleStation(deck, needle_station)
stock_solution_station = StockSolutionStation(deck, needle_station)

# create a list of samples, which contain the data we need for each iteration of the experiment
samples = [
    Sample('A1', 'B1', 2.0),  # fill sample vial in A1 with 2.0mL from stock vial in B1
    Sample('A2', 'B2', 1.0),  # fill sample vial in A2 with 1.0mL from stock vial in B2
    Sample('A3', 'B3', 3.0)   # fill sample vial in A3 with 3.0mL from stock vial in B3
]


# check to see if we are running this as a script instead of importing it
if __name__ == '__main__':
    # loop through the samples we have defined
    for sample in samples:
        # pull XX mL of solution into a fresh needle
        stock_solution_station.pump_from_vial(sample.stock_solution_index, sample.stock_volume_ml)
        # dispense into sample vial
        sample_station.pump_to_vial(sample.sample_index, sample.stock_volume_ml)
        # pickup and weigh sample vial
        sample_station.pickup_vial(sample.sample_index)
        sample.weight = weigh_station.weigh_vial()  # save the vial weight back into the sample data
        # place sample vial back in tray
        sample_station.place_vial(sample.sample_index)
