from north_robots.components import Location, GridTray
from north_robots.managers import NeedleTrayManager


class NeedleStation:
    """
    The NeedleStation class is an example of how to manage picking up and removing needles
    """

    def __init__(self, deck):
        self.deck = deck
        # create a new grid of locations, the origin location is found by driving the robot with the joystick spacing is in mm
        self.needle_tray_grid = GridTray(Location(50, 50, 100, rz=90), rows=3, columns=12, spacing=(6, 6))
        # create a manager, which controls the robot to pick up needles from trays
        self.needle_tray_manager = NeedleTrayManager(self.deck.n9, trays=[self.needle_tray_grid])

    def pickup_needle(self):
        self.needle_tray_manager.pickup()

    def remove_needle(self):
        # move the robot to the "approach location", which is a safe location in front of the needle remover

        # this location is also found by driving the robot with the joystick, setting probe=True will move the probe
        # to the given location instead of the gripper
        self.deck.n9.move(-100, -50, 300, probe=True)

        # move the robot to removal location, where the probe is touching the needle remover
        self.deck.n9.move(-90, -50, 300, probe=True)
        # move the robot upwards to remove the needle
        self.deck.n9.move(z=100, relative=True)  # this is a "relative" move that will move the arm 100mm upwards
        # move the arm out away from the remover
        self.deck.n9.move(-100, -50, probe=True)  # leaving out the z position will move the arm to the given x, y position without moving vertically
