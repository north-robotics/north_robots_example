from north_robots.n9 import N9Robot
from north_robots.components import Location


class WeighStation:
    """
    The WeighStation class manages weighing vials with the scale
    """
    def __init__(self, deck):
        self.deck = deck
        # this is the location where a vial in the gripper is sitting on the scale
        self.weigh_location = Location(100, 50, 100)
        # this is a location above the scale that high enough that we can move to and from it without crashing
        self.safe_location = Location(100, 50, 250)

    def weigh_vial(self):
        # move the arm and vial above the scale, the order will split the movement into separate vertical and horizontal movements
        self.deck.n9.move_to_location(self.safe_location, order=N9Robot.MOVE_Z_XY)
        # tare the scale before we place the vial on it
        self.deck.scale.tare()
        # place the vial on the scale and open the gripper
        self.deck.n9.move_to_location(self.weigh_location)
        self.deck.n9.gripper_output.open()
        # weigh the vial and then close the gripper
        weight = self.deck.scale.weigh()
        self.deck.n9.gripper_output.close()
        # move the vial above the scale
        self.deck.n9.move_to_location(self.safe_location)

        return weight
