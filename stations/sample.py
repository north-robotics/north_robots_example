from north_robots.components import Location, GridTray
from north_robots.managers import VialTrayManager


class SampleStation:
    """
    The SampleStation manages a tray of sample vials and allows you to pump liquid into a sample vial and weigh it
    """

    def __init__(self, deck, needle_station):
        self.deck = deck
        self.needle_station = needle_station
        # create a GridTray that defines the locations in the sample vial tray
        self.vial_tray_grid = GridTray(Location(0, 200, 100, rz=180), rows=4, columns=5, spacing=(10, 10))
        # setup a tray manager to manage picking up, placing down, and piercing vials
        self.vial_tray_manager = VialTrayManager(self.deck.n9, trays=[self.vial_tray_grid])

    def pump_to_vial(self, index, volume_ml):
        # move the robot to pierce the vial with the needle, assuming a needle is already attached
        self.vial_tray_manager.pierce(index)
        # pump liquid from the needle into the sample vial
        self.deck.pump.pump_ml(volume_ml)  # use a positive volume to push from the pump
        # move the needle out of the vial
        self.vial_tray_manager.move_to_safe_location(index)
        # remove the needle from the probe
        self.needle_station.remove_needle()

    def pickup_vial(self, index):
        # pickup a sample vial at the given tray index
        self.vial_tray_manager.pickup(index)

    def place_vial(self, index):
        # place a sample vial into the given tray index
        self.vial_tray_manager.place(index)
