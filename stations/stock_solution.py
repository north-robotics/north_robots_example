from north_robots.components import Location, GridTray
from north_robots.managers import VialTrayManager


class StockSolutionStation:
    """
    The StockSolutionStation class manages a tray of vials filled with stock solution and allows you to pump stock
    solutions into a needle / sample loop
    """

    def __init__(self, deck, needle_station):
        self.deck = deck
        self.needle_station = needle_station
        # create a GridTray that defines the locations in the stock vial tray
        self.vial_tray_grid = GridTray(Location(0, 200, 100, rz=180), rows=4, columns=5, spacing=(10, 10))
        # setup a tray manager to manage picking up, placing down, and piercing vials
        self.vial_tray_manager = VialTrayManager(self.deck.n9, trays=[self.vial_tray_grid])

    def pump_from_vial(self, index, volume_ml):
        # pickup a fresh needle
        self.needle_station.pickup_needle()
        # pierce the stock solution vial with a needle
        self.vial_tray_manager.pierce(index)
        # pump the given volume of stock solution into the needle / sample loop
        self.deck.pump.pump_ml(-volume_ml)  # use a negative volume to pull into the pump
        # move the needle out of the vial
        self.vial_tray_manager.move_to_safe_location(index)
